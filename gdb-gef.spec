%global forgeurl https://github.com/hugsy/gef

Name:           gdb-gef
Version:        2024.06

%forgemeta

Release:        5%{?dist}
Summary:        GEF (GDB Enhanced Features)

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Source1:        gdb-gef
# https://github.com/hugsy/gef/pull/1094
Patch0:         gef-got-audit.patch
BuildArch:      noarch

Requires:       gdb
Requires:       file
Requires:       binutils
Requires:       procps-ng
Requires:       python3

BuildRequires:  gdb
BuildRequires:  gdb-gdbserver
BuildRequires:  file
BuildRequires:  binutils
BuildRequires:  procps-ng
BuildRequires:  python3
BuildRequires:  python3-pylint
BuildRequires:  python3-pytest
BuildRequires:  python3-pytest-benchmark
BuildRequires:  python3-pytest-cov
BuildRequires:  python3-pytest-xdist
BuildRequires:  python3-coverage
BuildRequires:  python3-rpyc
BuildRequires:  python3-requests
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  sed
BuildRequires:  qemu-user
BuildRequires:  git


%description
GEF (pronounced ʤɛf - "Jeff") is a set of commands for x86/64, ARM,
MIPS, PowerPC and SPARC to assist exploit developers and
reverse-engineers when using old school GDB. It provides additional
features to GDB using the Python API to assist during the process of
dynamic analysis and exploit development. Application developers will
also benefit from it, as GEF lifts a great part of regular GDB
obscurity, avoiding repeating traditional commands, or bringing out
the relevant information from the debugging runtime.


%prep
%forgesetup
%patch 0 -p1


%install
mkdir -p %{buildroot}/%{_libdir}/gdb
cp gef.py %{buildroot}/%{_libdir}/gdb/gef.py

sed -e "s:@libdir@:%{_libdir}:g" < %{SOURCE1} > %{SOURCE1}.sh
mkdir -p %{buildroot}/%{_bindir}
install -m 0755 %{SOURCE1}.sh %{buildroot}/%{_bindir}/gdb-gef


%check
make -C tests/binaries
python3 -m pytest -v -m "not benchmark" -m "not online" tests/


%files
%license LICENSE
%doc docs README.md
%dir %{_libdir}/gdb
%{_libdir}/gdb/gef.py
%{_bindir}/gdb-gef


%changelog
* Thu Jun 20 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.06-5
- Update gef-got-audit.patch for new release

* Thu Jun 20 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.06-4
- Update source tarball. (gordon.messmer@gmail.com)

* Wed Jun 19 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.06-3
- Fix source

* Wed Jun 19 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.06-2
- Build 2024.06

* Wed Jun 19 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.06-1
- Build 2024.06

* Sun May 05 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.01-4
- Build noarch package.

* Sat Apr 27 2024 Gordon Messmer <gordon.messmer@gmail.com> 2024.01-2
- new package built with tito

* Wed Apr 24 2024 Gordon Messmer <gordon.messmer@gmail.com>
- Package gef 2024.01
